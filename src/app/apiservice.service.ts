import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  constructor(private http: HttpClient) { }

  getLists() {
    return this.http.get('https://jsonplaceholder.typicode.com/todos');
  }
  postList(product){
      return this.http.post('https://jsonplaceholder.typicode.com/posts', product);
   }
}
 