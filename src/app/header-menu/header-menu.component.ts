import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../apiservice.service';
import { FormControl, FormGroup, Validators} from '@angular/forms'

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.css']
})
export class HeaderMenuComponent implements OnInit {

  test:any=[];

  heroForm = new FormGroup({
    title: new FormControl(),
    completed: new FormControl(),
  });

  constructor(private http :ApiserviceService) { }

  ngOnInit() {
    this.http.getLists().subscribe((data )=>{
      this.test=data;
      console.log(data);
      console.log(Object.keys(this.test));
    } );
  }
  product={
    title:"",
    completed:""
  } 

  onSubmit(){
    this.http.postList(this.product).subscribe((product) => {
      this.test.unshift(this.product);
      console.log(this.test)
    }
    );
    
   
   
}

}